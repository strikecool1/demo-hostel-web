<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckAuth
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('user_id')) 
        {
            echo '<script>location.replace("/");</script>'; exit;
        } 
        else
        {
                $user = DB::table('Users')->where(['id' => session('user_id')])->first();
             
                   /* DB::table('Users')->where('id', session('user_id'))->update(['last_active' => date("Y-m-d H:i:s",time())]);*/
                    $role = DB::table('roles')
                        ->leftJoin('userRoles', 'userRoles.idRole', '=', 'Roles.id')
                        ->select('Roles.*')
                        ->where('userRoles.idUser', session('user_id') )->first();
                    session(['role_id' => $role->name]);
                    if ($role->name == 'Администратор')
                        return $next($request);
                    else
                    {
                        AuthCheck::logOut($request);
                    }
                
        }
    }

    function logOut($request)
    {
        $request->session()->getHandler()->destroy($request->session()->getID());
        echo '<script>location.replace("/");</script>'; exit;
    }
}
