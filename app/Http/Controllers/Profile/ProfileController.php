<?php
namespace App\Http\Controllers\Profile;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
/**
 * Перенаправление index
 */

class ProfileController extends Controller
{

   public function Profile_Stud_index(Request $request, $idPers)
   {
     if (session()->has('user_id'))
        {

                $queryPersonProfile = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Specialty', 'Specialty.id', '=', 'Group.idSpecialty')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)
                        ->select
                        (
                            DB::raw
                            (
                                'cast(birthday as datetime) as birth,
                                Students.surname as FirstName,
                                Students.name as Name,
                                Students.patronymic as LastName,
                                Students.photo as photo,
                                [Group].[name] as GroupName,
                                [Group].numberCourse as numberCourse,
                                Departaments.fullName as Inst,
                                Specialty.name as Spes,

                                Section.nameSection as nameSection,
                                Rooms.numberRoom  as numberRoom,
                                Propusk.status as status,
                                Buildings.name as Building,
                                Propusk.lastchange as lastchange,
                                Propusk.numberPropusk as num_propusk,
                                Propusk.blocked as block'
                            )
                        )
                        ->first();

                        $move_person = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->leftjoin('Move','Move.idPersonRooms', '=', 'Propusk.id')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)
                        ->select
                        (
                             DB::raw
                            (
                                'cast(Move.DateTimeMove as datetime) as date_move,
                                 Move.TypeStatus as type_status
                                '
                            )

                        )
                        ->orderBy('date_move', 'desc')
                        ->get();
                        //Расчет возраста студента
                        //
                        //$start = Carbon::createFromFormat('Y-m-d', $queryPersonProfile->birth)->format('d/m/Y');
                       $age = Carbon::parse($queryPersonProfile->birth)->diffInYears();
                      // dd($start);
            
            return view
            (
                'Profile.ProfileStud',
                [
                    'user_name' => session('user_name'),
                    'role' => session('role_id'),
                    'person_file' => $queryPersonProfile,
                    'date_person' => $age,
                    'move_' => $move_person,
                    'title' => "Личная карточка",
                    'back' => 'Главное'

                ]
            );
        }
        else
        {
            return redirect('/');
        }
   }

   public function Profile_Work_index(Request $request, $idPers)
   {
     if (session()->has('user_id'))
        {

                $queryPersonProfile = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('WorkerPosition', 'Workers.id', '=', 'WorkerPosition.idWorker')
                        ->join('DepartamentsPositions', 'DepartamentsPositions.id', '=', 'WorkerPosition.idDepartamentPosition')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsPositions.idDepartament')
                        ->join('Position', 'Position.id', '=', 'DepartamentsPositions.idPosition')
                         ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)

                        ->select
                        (
                             DB::raw
                            (
                            'PersonRooms.id as idPers,
                            Workers.surname as FirstName,
                            Workers.name as Name,
                            Workers.patronymic as LastName,
                            Workers.photo as photo,
                            cast(birthday as datetime) as birth,
                            Departaments.fullName as Dep,
                            Position.fullName as Pos,
                            Section.nameSection as nameSection,
                            Rooms.numberRoom  as numberRoom,
                            Propusk.status as status,
                            Buildings.name as Building,
                            Propusk.lastchange as lastchange,
                            Propusk.numberPropusk as num_propusk,
                            Propusk.blocked as block '
                            )

                        )
                        ->first();


                        $move_person = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->leftjoin('Move','Move.idPersonRooms', '=', 'Propusk.id')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)
                        ->select
                        (
                             DB::raw
                            (
                                'cast(Move.DateTimeMove as datetime) as date_move,
                                 Move.TypeStatus as type_status
                                '
                            )

                        )
                        ->orderBy('date_move', 'desc')
                        ->get();
                        //Расчет возраста студента
                        //$test = Carbon::parse($queryPersonProfile->birth)->format('d.m.Y');
                       $age = Carbon::parse($queryPersonProfile->birth)->diffInYears();

            
            return view
            (
                'Profile.ProfileWork',
                [
                    'user_name' => session('user_name'),
                    'role' => session('role_id'),
                    'person_file' => $queryPersonProfile,
                    'date_person' => $age,
                    'move_' => $move_person,
                    'title' => "Личная карточка",
                    'back' => 'Главное'

                ]
            );
        }
        else
        {
            return redirect('/');
        }
   }

   public function Profile_Imm_index(Request $request, $idPers)
   {
     if (session()->has('user_id'))
        {

                $queryPersonProfile = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants','Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)

                        ->select
                        (
                              DB::raw
                            (
                                 'PersonRooms.id as idPers,
                            Immigrants.surname as FirstName,
                            Immigrants.name as Name,
                            Immigrants.photo as photo,
                            Immigrants.patronymic as LastName,
                            Immigrants.addressCity as adressCity,
                            Immigrants.addressStreet as adressStreet,
                            Immigrants.addressHomeNumber as adressHome,
                            cast(Immigrants.birthday as datetime) as birth,
                            Section.nameSection as nameSection,
                            Rooms.numberRoom  as numberRoom,
                            Propusk.status as status,
                            Buildings.name as Building,
                            Propusk.lastchange as lastchange,
                            Propusk.numberPropusk as num_propusk,
                            Propusk.blocked as block '

                            )
                           

                        )
                        ->first();

                        $move_person = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->leftjoin('Move','Move.idPersonRooms', '=', 'Propusk.id')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('PersonRooms.id', '=', $idPers)
                        ->select
                        (
                             DB::raw
                            (
                                'cast(Move.DateTimeMove as datetime) as date_move,
                                 Move.TypeStatus as type_status
                                '
                            )

                        )
                        ->orderBy('date_move', 'desc')
                        ->get();
                        //Расчет возраста студента
                       $age = Carbon::parse($queryPersonProfile->birth)->diffInYears();
            
            return view
            (
                'Profile.ProfileImm',
                [
                    'user_name' => session('user_name'),
                    'role' => session('role_id'),
                    'person_file' => $queryPersonProfile,
                    'date_person' => $age,
                    'move_' => $move_person,
                    'title' => "Личная карточка",
                    'back' => 'Главное'

                ]
            );
        }
        else
        {
            return redirect('/');
        }
   }



}
