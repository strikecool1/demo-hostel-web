<?php
namespace App\Http\Controllers\Tables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;


class Table_Students_Controller extends Controller
{
    public function deleteStudent(Request $request)
    {
        $id_stud = $request->id_stud;
        DB::table('Students')
        ->where('id', $id_stud)
        ->delete();
            return redirect('/Table_Students');
    }

    public function insertStudent(Request $request)
    {

        //Generate Random Number
        $randomString = random_int(10000000,50000000);
        $number_propusk = DB::table('Propusk')->where('numberPropusk',$randomString)->first();

        $id_stud = $request->id_stud;
        $id_dep = $request->id_dep;

        $newPerson = DB::table('PersonRooms')->insertGetId(
        [
            'idStudGroup' => $id_stud,
            'idDepartametHostel' => $id_dep,
            'status' => 'S',
            'dateSattlement' => Carbon::now(),
            'dateInsert' =>  Carbon::now()
        ]);
        
        if($number_propusk == null)
        {
            DB::table('Propusk')->insert(
            [
                'idPersonRooms' => $newPerson,
                'status' => 'F',
                'blocked' => 'F',
                'numberPropusk' => $randomString
            ]);
        }
        /*else
        {
            return $randomString;
        }*/
          
    }
    
    
		public function Table_Student_index(Request $request)
        {
            if (session()->has('user_id'))                   
            {  
                
                return view
                (
                    'Tables.TableStudent', 
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id')
                    ]
                );
            }
            else
            {
                return redirect('/');
            }       
        }
        

        public function loadStudentTable(Request $request)
        {
            $search = $request->search;
            
            if (session()->has('user_id'))                   
            { 

               $item_stud = DB::table('Students')
                        ->join('StudentsGroup', 'StudentsGroup.idStudent', '=', 'Students.id')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('Specialty', 'Specialty.id', '=', 'Group.idSpecialty')
                        ->join('Departaments', 'Departaments.id', '=', 'Specialty.idDepartament')
                        ->select
                        (
                            'Students.id as id',
                            'StudentsGroup.id as id_stud',
                            'Students.surname as FirstName',
                            'Students.name as Name',
                            'Students.patronymic as LastName',
                            'Group.name as GroupName',
                            'Group.numberCourse as numberCourse',
                            'Specialty.name as nameSpecialty',
                            'Departaments.fullName as nameDep'

                        )
                        ->where('Students.surname','Like', "%".$search."%")
                        ->get();


                return view
                (
                    'Ajax.studentTable', 
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'item_student' => $item_stud
                    
                    ]
                );
            }
            else
            {
                return redirect('/');
            } 
        }
        public function loadRoomTable(Request $request)
        {
            $search = $request->search;

            if (session()->has('user_id'))                   
            {             
               $item_stud = DB::table('DepartamentsHostel')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Buildings.name as Building',
                            'DepartamentsHostel.id as id_dh'
                        )
                        ->where('Rooms.numberRoom','Like', "%".$search."%")
                        ->get();


                return view
                (
                    'Ajax.roomTable', 
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'item_student' => $item_stud
                    
                    ]
                );
            }
            else
            {
                return redirect('/');
            } 
        }


}