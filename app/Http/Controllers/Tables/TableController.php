<?php
namespace App\Http\Controllers\Tables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Перенаправление index
 */

class TableController extends Controller
{
    public function deletePerson( $idPers )
    {
        $id_pers = $request->id_pers;
        DB::table('PersonRooms')
        ->where('id', $id_pers)
        ->delete();
            return redirect('/Table_Person');
    }

    public function updatePerson(Request $request)
    {
        $id_pers = $request->id_pers;
        $id_dep = $request->id_dep;
        DB::table('PersonRooms')
        ->where('id', $id_pers)
        ->update(['PersonRooms.idDepartametHostel'=>$id_dep]);
    }


		public function Table_index(Request $request)
        {
            if (session()->has('user_id'))
            {
                return view
                (
                    'Tables.Table',
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'title' => 'Поиск студентов(заселённых)',
                        'back' => 'Главное'
                    ]
                );
            }
            else
            {
                return redirect('/');
            }
        }


        public function loadMainTable(Request $request)
        {
            $search = $request->search;

            if (session()->has('user_id'))
            {

               $item_stud = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'PersonRooms.id as idPers',
                            'Students.surname as FirstName',
                            'Students.name as Name',
                            'Students.patronymic as LastName',
                            'Group.name as GroupName',
                            'Group.numberCourse as numberCourse',
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Propusk.status as status',
                            'Buildings.name as Building',
                            'Propusk.lastchange as lastchange'

                        )
                        ->where('Students.surname','Like', "%".$search."%")
                        ->get();


                return view
                (
                    'Ajax.mainTable',
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'item_student' => $item_stud

                    ]
                );
            }
            else
            {
                return redirect('/');
            }
        }
        public function loadRoomTable(Request $request)
        {
            $search = $request->search;

            if (session()->has('user_id'))
            {
               $item_stud = DB::table('DepartamentsHostel')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Buildings.name as Building',
                            'DepartamentsHostel.id as id_dh'
                        )
                        ->where('Rooms.numberRoom','Like', "%".$search."%")
                        ->get();


                return view
                (
                    'Ajax.roomTable',
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'item_student' => $item_stud

                    ]
                );
            }
            else
            {
                return redirect('/');
            }
        }


}
