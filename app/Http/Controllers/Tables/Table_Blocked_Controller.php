<?php
namespace App\Http\Controllers\Tables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Перенаправление index
 */

class Table_Blocked_Controller extends Controller
{

    public function updateBlocked (Request $request)
    {
        $id_pers = $request->id_pers;     
        DB::table('Propusk')
        ->where('idPersonRooms', $id_pers)
        ->update(['Propusk.blocked'=> 'F']);
    }
   
		public function Table_index(Request $request)
        {
            if (session()->has('user_id'))                   
            { 

                 $person_block = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Propusk.blocked','T')
                        ->select
                        (
                            'PersonRooms.id as idPers',
                            'Students.surname as FirstName',
                            'Students.name as Name',
                            'Students.patronymic as LastName',
                            'Buildings.name as Building',
                            'Section.nameSection as nameSection',
                            'Propusk.blocked as is_block',
                            'Rooms.numberRoom as numRoom'
                        )
                         ->paginate(10);
                      

                return view
                (
                    'Tables.TableBlocked', 
                    [
                        'id' => session('user_id'),
                        'role' => session('role_id'),
                        'blocked_person' => $person_block
                    ]
                );
            }
            else
            {
                return redirect('/');
            }       
        }
        

       
      


}