<?php
namespace App\Http\Controllers\Hostel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class HostelController_7 extends Controller
{
  public function Hostel_7_Student_index(Request $request)
   {
     if (session()->has('user_id') )
        {
            $role = session('role_id');
            $users = session('user_name');
          return view
          (
              'Hostels.Hostel_7_S',
              [
                'role' => $role,
                'user_name' => $users,
                'title' => 'Общежитие 7',
                'back' => 'Основная информация'
              ]
          );

        }
        else
        {
            return redirect('/');
        }
   }

    public function Hostel_7_Worker_index(Request $request)
   {
     if (session()->has('user_id') )
        {
            $role = session('role_id');
            $users = session('user_name');
          return view
          (
              'Hostels.Hostel_7_W',
              [
                'role' => $role,
                'user_name' => $users,
                'title' => 'Общежитие 7',
                'back' => 'Основная информация'
              ]
          );

        }
        else
        {
            return redirect('/');
        }
   }

    public function Hostel_7_Immigrant_index(Request $request)
   {
     if (session()->has('user_id') )
        {
            $role = session('role_id');
            $users = session('user_name');
          return view
          (
              'Hostels.Hostel_7_I',
              [
                'role' => $role,
                'user_name' => $users,
                'title' => 'Общежитие 7',
                'back' => 'Основная информация'
              ]
          );

        }
        else
        {
            return redirect('/');
        }
   }

   //------------- Загрузка студентов ------------------------------
   public function loadItem_Student(Request $request)
   {
        $item_stud = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'PersonRooms.id as idPers',
                            'Students.surname as FirstName',
                            'Students.name as Name',
                            'Students.patronymic as LastName',
                            'Departaments.fullName as Inst',
                            'Group.name as Group',
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Propusk.status as status',
                            'Buildings.name as Building',
                            'Propusk.lastchange as lastchange'

                        )
                        ->where('Buildings.id',12)
                        ->get();

        $k = [];
        $i = 0;
        foreach ($item_stud as $key) 
        {

            $k +=[$i =>
            [
           // $key->idPers ,
            $key->FirstName.' '.$key->Name.' '.$key->LastName,
            $key->nameSection, 
            $key->numberRoom,
            $key->Inst,
            $key->Group,
            $key->status,
            Carbon::parse( $key->lastchange)->format('d.m.Y H:i:s'),
            '<div class="btn-group"> <a href="/Profile_Stud/'.$key->idPers.'" class="btn btn-info" title="Открыть"><i class="fas fa-eye"></i></a><a href="#" class="btn btn-danger" title="Удалить"><i class="fas fa-trash"></i></a></div>'
          ]
          ];
            $i++;
        }
        $arr=array
        (
            "data" =>
               $k
        );
        header("Content-type: application/json; charset=utf-8");
        return json_encode($arr, true);
   }

    public function loadItem_Worker(Request $request)
   {
        $item_work = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('WorkerPosition', 'Workers.id', '=', 'WorkerPosition.idWorker')
                        ->join('DepartamentsPositions', 'DepartamentsPositions.id', '=', 'WorkerPosition.idDepartamentPosition')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsPositions.idDepartament')
                        ->join('Position', 'Position.id', '=', 'DepartamentsPositions.idPosition')
                         ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'PersonRooms.id as idPers',
                            'Workers.surname as FirstName',
                            'Workers.name as Name',
                            'Workers.patronymic as LastName',
                            'Departaments.fullName as Dep',
                            'Position.fullName as Pos',
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Propusk.status as status',
                            'Buildings.name as Building',
                            'Propusk.lastchange as lastchange'

                        )
                        ->where('Buildings.id',12)
                        ->where('WorkerPosition.isMain','T')
                        ->get();

        $k = [];
        $i = 0;
        foreach ($item_work as $key) 
        {

            $k +=[$i =>
            [
             // $key->idPers ,
             $key->FirstName.' '.$key->Name.' '.$key->LastName,
             $key->nameSection, 
            $key->numberRoom,
            $key->Dep,
            $key->Pos,
            $key->status,
            Carbon::parse( $key->lastchange)->format('d.m.Y H:i:s'),
            '<div class="btn-group"> <a href="/Profile_Work/'.$key->idPers.'" class="btn btn-info" title="Открыть"><i class="fas fa-eye"></i></a>
            <a href="#" class="btn btn-danger" title="Удалить"><i class="fas fa-trash"></i></a></div>
            '
          ]
          ];
            $i++;
        }
        $arr=array
        (
            "data" =>
               $k
        );
        header("Content-type: application/json; charset=utf-8");
        return json_encode($arr, true);
   }

    public function loadItem_Immigrant(Request $request)
   {
        $item_imm = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants','Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')

                        ->select
                        (
                            'PersonRooms.id as idPers',
                            'Immigrants.surname as FirstName',
                            'Immigrants.name as Name',
                            'Immigrants.patronymic as LastName',
                            'Section.nameSection as nameSection',
                            'Rooms.numberRoom  as numberRoom',
                            'Propusk.status as status',
                            'Buildings.name as Building',
                            'Propusk.lastchange as lastchange'

                        )
                        ->where('Buildings.id',12)
                        ->get();

        $k = [];
        $i = 0;
        foreach ($item_imm as $key) 
        {

            $k +=[$i =>
            [
              //$key->idPers ,
             $key->FirstName.' '.$key->Name.' '.$key->LastName,
             $key->nameSection, 
            $key->numberRoom,
            $key->status,
            Carbon::parse( $key->lastchange)->format('d.m.Y H:i:s'),
            '<div class="btn-group"> <a href="/Profile_Imm/'.$key->idPers.'" class="btn btn-info" title="Открыть"><i class="fas fa-eye"></i></a><a href="#" class="btn btn-danger" title="Удалить"><i class="fas fa-trash"></i></a></div>'
          ]
          ];
            $i++;
        }
        $arr=array
        (
            "data" =>
               $k
        );
        header("Content-type: application/json; charset=utf-8");
        return json_encode($arr, true);
   }



}
