<?php
namespace App\Http\Controllers\Contact;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


/**
 * Перенаправление index
 */

class ContactController extends Controller
{

		public function Contact_index(Request $request)
        {

                if (session()->has('user_id'))                   
                {
                        $role = session('role_id');
                        $users = session('user_name');


                    return view
                    (
                        'Contact.Contact', 
                        [
                            'role' => $role,
                            'user_name' => $users,
                            'title' => 'Контакты',
                            'back' => 'Основная'
                          
                        ]
                    );
                }
                else
                {
                    return redirect('/');
                }
       
    }

}