<?php
namespace App\Http\Controllers\Chart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

/**
 * Перенаправление index
 */

class ChartController extends Controller
{
		public function Chart_Main_index(Request $request)
            {
                    if (session()->has('user_id'))
                    {
				//------------------ Количество студентов  ------------------------------------
                        $all_obsh_2 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->count();

                         $all_obsh_3 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->count();

                         $all_obsh_4 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->count();

                         $all_obsh_5 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->count();

                         $all_obsh_7 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->count();

                         //---------------------------  -----------------------------------------------
                        $blocked_obsh_2 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('Propusk.blocked','T')
                        ->count();

                        $blocked_obsh_3 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('Propusk.blocked','T')
                        ->count();

                        $blocked_obsh_4 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('Propusk.blocked','T')
                        ->count();

                        $blocked_obsh_5 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('Propusk.blocked','T')
                        ->count();

                        $blocked_obsh_7 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('Propusk.blocked','T')
                        ->count();
												// --------------------- Количество сотрудников ----------------------------
                        $sotr_obsh_2 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->count();
                        $sotr_obsh_3 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->count();
                        $sotr_obsh_4 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->count();
                        $sotr_obsh_5 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->count();
                        $sotr_obsh_7 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->count();

												//---------------------- Количество переселенцев ----------------------------------//
                         $imm_obsh_2 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->count();
                        $imm_obsh_3 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->count();
                        $imm_obsh_4 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->count();
                        $imm_obsh_5 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->count();
                        $imm_obsh_7 = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->count();
			//------------------------ Статистика сколько зашло за месяц (Март) -----------------------//
			$count_Mart_Input = DB::table('PersonRooms')
			->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
			->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
			->where('Move.DateTimeMove', '>=' , '2020-03-01')
			->where('Move.DateTimeMove', '<' , '2020-03-31')
			->where('Move.TypeStatus','Зашел')
                        ->count();

			$count_Mart_Output = DB::table('PersonRooms')
			->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
			->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-03-01')
			->where('Move.DateTimeMove', '<' , '2020-03-31')
			->where('Move.TypeStatus','Вышел')
                        ->count();

                        //------------------------ Статистика сколько зашло за месяц (Апрель) -----------------------//
			$count_Aprl_Input = DB::table('PersonRooms')
			->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
			->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
			->where('Move.DateTimeMove', '>=' , '2020-04-01')
			->where('Move.DateTimeMove', '<' , '2020-04-30')
			->where('Move.TypeStatus','Зашел')
                        ->count();

			$count_Aprl_Output = DB::table('PersonRooms')
			->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
			->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-04-01')
			->where('Move.DateTimeMove', '<' , '2020-04-30')
			->where('Move.TypeStatus','Вышел')
                        ->count();

                             //------------------------ Статистика сколько зашло за месяц (Май) -----------------------//
                  $count_May_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-05-01')
                  ->where('Move.DateTimeMove', '<' , '2020-05-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_May_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-05-01')
                  ->where('Move.DateTimeMove', '<' , '2020-05-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();

                             //------------------------ Статистика сколько зашло за месяц (Июнь) -----------------------//
                  $count_Jun_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-06-01')
                  ->where('Move.DateTimeMove', '<' , '2020-06-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_Jun_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-06-01')
                  ->where('Move.DateTimeMove', '<' , '2020-06-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();


                          //------------------------ Статистика сколько зашло за месяц (Июль) -----------------------//
                  $count_Jul_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-07-01')
                  ->where('Move.DateTimeMove', '<' , '2020-07-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_Jul_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-07-01')
                  ->where('Move.DateTimeMove', '<' , '2020-07-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();

                           //------------------------ Статистика сколько зашло за месяц (Август) -----------------------//
                  $count_Aug_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-08-01')
                  ->where('Move.DateTimeMove', '<' , '2020-08-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_Aug_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-08-01')
                  ->where('Move.DateTimeMove', '<' , '2020-08-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();
                         //------------------------ Статистика сколько зашло за месяц (Сентябрь) -----------------------//
                  $count_Sen_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-09-01')
                  ->where('Move.DateTimeMove', '<' , '2020-09-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_Sen_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-09-01')
                  ->where('Move.DateTimeMove', '<' , '2020-09-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();
                         //------------------------ Статистика сколько зашло за месяц (Октябрь) -----------------------//
                  $count_Oct_Input = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                  ->where('Move.DateTimeMove', '>=' , '2020-10-01')
                  ->where('Move.DateTimeMove', '<' , '2020-10-30')
                  ->where('Move.TypeStatus','Зашел')
                        ->count();

                  $count_Oct_Output = DB::table('PersonRooms')
                  ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                  ->join('Move', 'Move.idPersonRooms', '=', 'Propusk.id')
                        ->where('Move.DateTimeMove', '>=' , '2020-10-01')
                  ->where('Move.DateTimeMove', '<' , '2020-10-30')
                  ->where('Move.TypeStatus','Вышел')
                        ->count();

            return view
            (
                'Charts.ChartMain',
                [
                'user_name' => session('user_name'),
                'role' => session('role_id'),
                'chart_all_obsh_2' =>     $all_obsh_2,
                'chart_all_obsh_3' =>     $all_obsh_3,
                'chart_all_obsh_4' =>     $all_obsh_4,
                'chart_all_obsh_5' =>     $all_obsh_5,
                'chart_all_obsh_7' =>     $all_obsh_7,
                'chart_blocked_obsh_2' => $blocked_obsh_2,
                'chart_blocked_obsh_3' => $blocked_obsh_3,
                'chart_blocked_obsh_4' => $blocked_obsh_4,
                'chart_blocked_obsh_5' => $blocked_obsh_5,
                'chart_blocked_obsh_7' => $blocked_obsh_7,
                 'chart_sotr_obsh_2' => $sotr_obsh_2,
                 'chart_sotr_obsh_3' => $sotr_obsh_3,
                 'chart_sotr_obsh_4' => $sotr_obsh_4,
                 'chart_sotr_obsh_5' => $sotr_obsh_5,
                 'chart_sotr_obsh_7' => $sotr_obsh_7,
                 'chart_imm_obsh_2' => $imm_obsh_2,
                 'chart_imm_obsh_3' => $imm_obsh_3,
                 'chart_imm_obsh_4' => $imm_obsh_4,
                 'chart_imm_obsh_5' => $imm_obsh_5,
                 'chart_imm_obsh_7' => $imm_obsh_7,

		'count_input_mart' => $count_Mart_Input,
		'count_output_mart' => $count_Mart_Output,

            'count_input_apr' => $count_Aprl_Input,
		'count_output_apr' => $count_Aprl_Output,

            'count_input_may' => $count_May_Input,
            'count_output_may' => $count_May_Output,

            'count_input_jun' => $count_Jun_Input,
            'count_output_jun' => $count_Jun_Output,

            'count_input_jul' => $count_Jul_Input,
            'count_output_jul' => $count_Jul_Output,

            'count_input_aug' => $count_Aug_Input,
            'count_output_aug' => $count_Aug_Output,

            'count_input_sen' => $count_Sen_Input,
            'count_output_sen' => $count_Sen_Output,

            'count_input_oct' => $count_Oct_Input,
            'count_output_oct' => $count_Oct_Output,

                 'title' => 'Общая численность',
                 'back' => 'Главное'
                // 'chart_today' => $today
                ]
            );
        }
        else
        {
            return redirect('/');
        }

    }


    public function Chart_Global_index(Request $request)
            {
                    if (session()->has('user_id'))
                    {
                        //---------------------ОБЩЕЖИТИЕ №2---------------------------------------//
                        $hostel_2_Student_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','T')
                        ->count();

                         $hostel_2_Student_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_2_Worker_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_2_Worker_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_2_Immigrant_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_2_Immigrant_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','F')
                        ->count();

                        //------------------------------------------------------------------------------
                        //------------------------------3 общежитие ------------------------------------
                        $hostel_3_Student_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','T')
                        ->count();

                         $hostel_3_Student_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_3_Worker_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_3_Worker_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_3_Immigrant_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_3_Immigrant_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','F')
                        ->count();

                         //------------------------------4 общежитие ------------------------------------
                        $hostel_4_Student_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','T')
                        ->count();

                         $hostel_4_Student_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_4_Worker_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_4_Worker_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_4_Immigrant_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_4_Immigrant_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','F')
                        ->count();

                         //------------------------------5 общежитие ------------------------------------
                        $hostel_5_Student_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','T')
                        ->count();

                         $hostel_5_Student_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_5_Worker_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_5_Worker_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_5_Immigrant_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_5_Immigrant_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','F')
                        ->count();


                         //------------------------------7 общежитие ------------------------------------
                        $hostel_7_Student_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','T')
                        ->count();

                         $hostel_7_Student_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('PersonRooms.status','S')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_7_Worker_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_7_Worker_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Workers', 'Workers.id', '=', 'PersonRooms.idWorkers')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('PersonRooms.status','W')
                        ->where('Propusk.status','F')
                        ->count();

                         $hostel_7_Immigrant_True = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','T')
                        ->count();

                        $hostel_7_Immigrant_False = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('Immigrants', 'Immigrants.id', '=', 'PersonRooms.idImmigrant')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('PersonRooms.status','I')
                        ->where('Propusk.status','F')
                        ->count();
            


            return view
            (
                'Charts.ChartGlobal',
                [
                'user_name' => session('user_name'),
                'role' => session('role_id'), 
                'title' => 'Подробная статистика',
                'back' => 'Главное',

                'hostel_2_S_T' => $hostel_2_Student_True,
                'hostel_2_S_F' => $hostel_2_Student_False,

                'hostel_2_W_T' => $hostel_2_Worker_True,
                'hostel_2_W_F' => $hostel_2_Worker_False,

                'hostel_2_I_T' => $hostel_2_Immigrant_True,
                'hostel_2_I_F' => $hostel_2_Immigrant_False,

                'hostel_3_S_T' => $hostel_3_Student_True,
                'hostel_3_S_F' => $hostel_3_Student_False,

                'hostel_3_W_T' => $hostel_3_Worker_True,
                'hostel_3_W_F' => $hostel_3_Worker_False,

                'hostel_3_I_T' => $hostel_3_Immigrant_True,
                'hostel_3_I_F' => $hostel_3_Immigrant_False,


                'hostel_4_S_T' => $hostel_4_Student_True,
                'hostel_4_S_F' => $hostel_4_Student_False,

                'hostel_4_W_T' => $hostel_4_Worker_True,
                'hostel_4_W_F' => $hostel_4_Worker_False,

                'hostel_4_I_T' => $hostel_4_Immigrant_True,
                'hostel_4_I_F' => $hostel_4_Immigrant_False,


                'hostel_5_S_T' => $hostel_5_Student_True,
                'hostel_5_S_F' => $hostel_5_Student_False,

                'hostel_5_W_T' => $hostel_5_Worker_True,
                'hostel_5_W_F' => $hostel_5_Worker_False,

                'hostel_5_I_T' => $hostel_5_Immigrant_True,
                'hostel_5_I_F' => $hostel_5_Immigrant_False,

                'hostel_7_S_T' => $hostel_7_Student_True,
                'hostel_7_S_F' => $hostel_7_Student_False,

                'hostel_7_W_T' => $hostel_7_Worker_True,
                'hostel_7_W_F' => $hostel_7_Worker_False,

                'hostel_7_I_T' => $hostel_7_Immigrant_True,
                'hostel_7_I_F' => $hostel_7_Immigrant_False



                ]
            );
                
        }
        else
        {
            return redirect('/');
        }

    }
}
