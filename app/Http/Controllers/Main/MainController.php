<?php
namespace App\Http\Controllers\Main;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


/**
 * Перенаправление index
 */

class MainController extends Controller
{

		public function Main_index(Request $request)
        {

                if (session()->has('user_id'))                   
                {
                        $role = session('role_id');
                        $users = session('user_name');
                      

                        $count_all_person = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->count();
                        //--------- Количество заблокированных ---------------//
                         $count_all_blocked = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Propusk.blocked','T')
                        ->count();

                        $count_all_stud = DB::table('Students')
                        ->join('StudentsGroup','Students.id','=','StudentsGroup.idStudent')
                        ->join('Group','StudentsGroup.idGroup','=','Group.id')
                        ->count();

                        $count_host_2_T = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','T')
                        ->count();

                         $count_host_2_F = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',7)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','F')
                        ->count();

                        $count_host_3_T = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','T')
                        ->count();

                        $count_host_3_F = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',8)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','F')
                        ->count();


                        $count_host_4_T = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','T')
                        ->count();

                        $count_host_4_F = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',9)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','F')
                        ->count();

                         $count_host_5_T = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','T')
                        ->count();

                        $count_host_5_F = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',10)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','F')
                        ->count();

                         $count_host_7_T = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','T')
                        ->count();

                        $count_host_7_F = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->where('Buildings.id',12)
                        ->where('Propusk.blocked','F')
                        ->where('Propusk.status','F')
                        ->count();

                         $move_stud = DB::table('PersonRooms')
                        ->join('Propusk', 'PersonRooms.id', '=', 'Propusk.idPersonRooms')
                        ->join('StudentsGroup', 'StudentsGroup.id', '=', 'PersonRooms.idStudGroup')
                        ->join('Students', 'Students.id', '=', 'StudentsGroup.idStudent')
                        ->join('Group', 'Group.id', '=', 'StudentsGroup.idGroup')
                        ->join('DepartamentsHostel', 'DepartamentsHostel.id', '=', 'PersonRooms.idDepartametHostel')
                        ->join('Departaments', 'Departaments.id', '=', 'DepartamentsHostel.idDepartaments')
                        ->join('Rooms', 'Rooms.id', '=', 'DepartamentsHostel.idRooms')
                        ->join('Section', 'Section.id', '=', 'Rooms.idSection')
                        ->join('Hostel', 'Hostel.id', '=', 'Section.idHostel')
                        ->join('Buildings', 'Buildings.id', '=', 'Hostel.idBuildings')
                        ->join('Move','Move.idPersonRooms','=','Propusk.id')
                        ->select
                        (
                            'Students.surname as FirstName',
                            'Students.name as Name',
                            'Students.patronymic as LastName',
                            'Move.DateTimeMove as Date_Move',
                            'Buildings.name as Building',
                            'Move.TypeStatus as status'
                        )
                        ->orderBy('Move.DateTimeMove', 'desc')
                        ->paginate(10);


                    return view
                    (
                        'Main.Main', 
                        [
                            'role' => $role,
                            'user_name' => $users,
                            'all_person' =>  $count_all_person,
                            'all_blocked' => $count_all_blocked,
                            'all_student' => $count_all_stud,
                            'hostel_2_T' => $count_host_2_T,
                            'hostel_2_F' => $count_host_2_F,
                            'hostel_3_T' => $count_host_3_T,
                            'hostel_3_F' => $count_host_3_F,
                            'hostel_4_T' => $count_host_4_T,
                            'hostel_4_F' => $count_host_4_F,
                            'hostel_5_T' => $count_host_5_T,
                            'hostel_5_F' => $count_host_5_F,
                            'hostel_7_T' => $count_host_7_T,
                            'hostel_7_F' => $count_host_7_F,
                            'row_move' => $move_stud,
                            'title' => 'Главное',
                            'back' => 'Основная информация'
                        ]
                    );
                }
                else
                {
                    return redirect('/');
                }
       
    }

}