<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
   
   public function check_auth()
    {

        if (session()->has('user_id'))
        {
            return redirect('/Main');
        }
        else
        {
            return view('Login.login');
        }
    }

    /**
     * [Quit - разрывает сессию]
     */
    public function Quit()
    {
        session()->flush();
        return redirect('/');
    }

    /**
     * [Login Описание]
     * @param Request $request [Проверяем пользователя на логин -> в случаи успеха ->
     *  справниваем значение захешированного пароля->Переходим на view
     *  ]
     */
    public function Login(Request $request)
	{
        $log = trim($request->login);
        $pass = trim($request->password);

             $user = DB::table('Users')
            ->leftJoin('UserRoles', 'UserRoles.idUser', '=', 'Users.id')
            ->leftJoin('Roles', 'Roles.id', '=', 'UserRoles.idRole')
            ->leftJoin('Workers', 'Users.idWorker', '=', 'Workers.id')
            ->select('Roles.name as role_name','Users.*','Roles.name as r_name','Workers.name as w_name')
            ->where(['login' => $log])->first();//по сути на пароль и не нужна проверка

            if ($user != null) 
            {
                if (Hash::check($pass, $user->password)) 
                {
                    session
                    (
                        ['user_id' => $user->id,
                         'role_id' => $user->r_name,
                         'user_name' =>$user->w_name
                        ]
                    );
                }
            }


    return redirect('/Main');      
    }
}
