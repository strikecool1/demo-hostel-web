<?php

//----------Проверка сессии----------------------//
Route::get('/', 'Login\AuthController@check_auth');
//---------- Проверка логина и пароля ----------------------//
Route::post('/Login','Login\AuthController@Login');
//---------- Разблокировка студентов  ----------------------//
//
Route::get('/Table_Blocked/updateBlocked', 'Tables\Table_Blocked_Controller@updateBlocked');
Route::get('/DeletePerson/{idPers}', 'Hostel\HostelController_2@deletePerson');

//---------- Загрузка главного страницы приложения ----------------------//
Route::get('/Main','Main\MainController@Main_index')->name('main.Main')->middleware('check');
//----------Загрузка страницы графиков данных приложения----------------------//
Route::get('/ChartMain','Chart\ChartController@Chart_Main_index')->name('charts.chartMain')->middleware('check');
Route::get('/ChartGlobal','Chart\ChartController@Chart_Global_index')->name('charts.chartGlobal')->middleware('check');
//----------Загрузка страницы Общежитий  ----------------------//
//------------------2------------------------------------------//
Route::get('/Hostel_2/Student', 'Hostel\HostelController_2@Hostel_2_Student_index')->name('hostels.Hostel_2.S')->middleware('check');
Route::get('/Hostel_2/Worker', 'Hostel\HostelController_2@Hostel_2_Worker_index')->name('hostels.Hostel_2.W')->middleware('check');
Route::get('/Hostel_2/Immigrant', 'Hostel\HostelController_2@Hostel_2_Immigrant_index')->name('hostels.Hostel_2.I')->middleware('check');

//------------------3------------------------------------------//
Route::get('/Hostel_3/Student', 'Hostel\HostelController_3@Hostel_3_Student_index')->name('hostels.Hostel_3.S')->middleware('check');
Route::get('/Hostel_3/Worker', 'Hostel\HostelController_3@Hostel_3_Worker_index')->name('hostels.Hostel_3.W')->middleware('check');
Route::get('/Hostel_3/Immigrant', 'Hostel\HostelController_3@Hostel_3_Immigrant_index')->name('hostels.Hostel_3.I')->middleware('check');

//------------------4------------------------------------------//
Route::get('/Hostel_4/Student', 'Hostel\HostelController_4@Hostel_4_Student_index')->name('hostels.Hostel_4.S')->middleware('check');
Route::get('/Hostel_4/Worker', 'Hostel\HostelController_4@Hostel_4_Worker_index')->name('hostels.Hostel_4.W')->middleware('check');
Route::get('/Hostel_4/Immigrant', 'Hostel\HostelController_4@Hostel_4_Immigrant_index')->name('hostels.Hostel_4.I')->middleware('check');

//------------------5------------------------------------------//
Route::get('/Hostel_5/Student', 'Hostel\HostelController_5@Hostel_5_Student_index')->name('hostels.Hostel_5.S')->middleware('check');
Route::get('/Hostel_5/Worker', 'Hostel\HostelController_5@Hostel_5_Worker_index')->name('hostels.Hostel_5.W')->middleware('check');
Route::get('/Hostel_5/Immigrant', 'Hostel\HostelController_5@Hostel_5_Immigrant_index')->name('hostels.Hostel_5.I')->middleware('check');

//------------------7------------------------------------------//
Route::get('/Hostel_7/Student', 'Hostel\HostelController_7@Hostel_7_Student_index')->name('hostels.Hostel_7.S')->middleware('check');
Route::get('/Hostel_7/Worker', 'Hostel\HostelController_7@Hostel_7_Worker_index')->name('hostels.Hostel_7.W')->middleware('check');
Route::get('/Hostel_7/Immigrant', 'Hostel\HostelController_7@Hostel_7_Immigrant_index')->name('hostels.Hostel_7.I')->middleware('check');


//----------Загрузка страницы профиля студента  ----------------------//
Route::get('/Profile_Stud/{idPers}', 'Profile\ProfileController@Profile_Stud_index')->middleware('check');
Route::get('/Profile_Work/{idPers}', 'Profile\ProfileController@Profile_Work_index')->middleware('check');
Route::get('/Profile_Imm/{idPers}', 'Profile\ProfileController@Profile_Imm_index')->middleware('check');

//----------Загрузка страницы контаков  ----------------------//
Route::get('/Contact', 'Contact\ContactController@Contact_index')->middleware('check');

//----------Загрузка страницы заблокированных ----------------------//
Route::get('/Blocked','Blocked\BlockedController@Block_index')->name('blocked.table_blocked')->middleware('check');

//----------ОТОБРАЖЕНИЕ ДАННЫХ В ТАБЛИЦЕ ЧЕРЕЗ JSON ------------------------
// -----------  ОБЩЕЖИТИЕ №2 ------------------------------------
Route::get('/Hostel_2_S', 'Hostel\HostelController_2@loadItem_Student');
Route::get('/Hostel_2_W', 'Hostel\HostelController_2@loadItem_Worker');
Route::get('/Hostel_2_I', 'Hostel\HostelController_2@loadItem_Immigrant');

// -----------  ОБЩЕЖИТИЕ №3 ------------------------------------
Route::get('/Hostel_3_S', 'Hostel\HostelController_3@loadItem_Student');
Route::get('/Hostel_3_W', 'Hostel\HostelController_3@loadItem_Worker');
Route::get('/Hostel_3_I', 'Hostel\HostelController_3@loadItem_Immigrant');

// -----------  ОБЩЕЖИТИЕ №4 ------------------------------------
Route::get('/Hostel_4_S', 'Hostel\HostelController_4@loadItem_Student');
Route::get('/Hostel_4_W', 'Hostel\HostelController_4@loadItem_Worker');
Route::get('/Hostel_4_I', 'Hostel\HostelController_4@loadItem_Immigrant');

// -----------  ОБЩЕЖИТИЕ №5 ------------------------------------
Route::get('/Hostel_5_S', 'Hostel\HostelController_5@loadItem_Student');
Route::get('/Hostel_5_W', 'Hostel\HostelController_5@loadItem_Worker');
Route::get('/Hostel_5_I', 'Hostel\HostelController_5@loadItem_Immigrant');

// -----------  ОБЩЕЖИТИЕ №7 ------------------------------------
Route::get('/Hostel_7_S', 'Hostel\HostelController_7@loadItem_Student');
Route::get('/Hostel_7_W', 'Hostel\HostelController_7@loadItem_Worker');
Route::get('/Hostel_7_I', 'Hostel\HostelController_7@loadItem_Immigrant');

//----------ОТОБРАЖЕНИЕ ДАННЫХ (Заблокированных) В ТАБЛИЦЕ ЧЕРЕЗ JSON ------------------------
Route::get('/Blocked_S', 'Blocked\BlockedController@loadItem_Student');
Route::get('/Blocked_W', 'Blocked\BlockedController@loadItem_Worker');
Route::get('/Blocked_I', 'Blocked\BlockedController@loadItem_Immigrant');


//---------- Разрыв сессии ----------------------//
Route::get('/Quit', 'Login\AuthController@Quit');
