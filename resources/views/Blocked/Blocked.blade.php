@extends('layout.application')

@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/datatables.css') }}">
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

          <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Список</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

          <div class="card card-primary card-outline card-outline-tabs">
              <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-four-student-tab" data-toggle="pill" href="#custom-tabs-four-student" role="tab" aria-controls="custom-tabs-four-student" aria-selected="true" style="">Студенты</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false" style="">Сотрудники</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false" style="">Переселенцы</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-four-tabContent">
                  <div class="tab-pane fade active show" id="custom-tabs-four-student" role="tabpanel" aria-labelledby="custom-tabs-four-student-tab">

                     <table id="tableStud" class="table table-striped table-bordered">
                    <thead>
                    <tr>

                      <th width="20%">Ф.И.О.</th>
                        <th width="10%">Секция</th>
                        <th width="10%">Комната</th>
                        <th width="15%">Институт</th>
                        <th width="20%">Группа</th>
                        <th width="5%">Статус</th>
                        <th width="10%">Последняя активиность</th>
                        <th width="10%">Действия</th>
                       
                    </tr>
                    </thead>
                </table>

                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                    <table id="tableWorker" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        
                        <th width="20%">Ф.И.О.</th>
                        <th width="10%">Секция</th>
                        <th width="10%">Комната</th>
                        <th width="15%">Отдел</th>
                        <th width="20%">Должность</th>
                        <th width="5%">Статус</th>
                        <th width="10%">Последняя активиность</th>
                        <th width="10%">Действия</th>
                    </tr>
                    </thead>
                </table>

                  </div>
                  <div class="tab-pane fade" id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">

                   <table id="tableImm" class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        
                        <th width="35%">Ф.И.О.</th>
                        <th width="10%">Секция</th>
                        <th width="10%">Комната</th>
                        <th width="5%">статус</th>
                        <th width="35%">Последняя активность</th>
                        <th width="5%">Действия</th>
                    </tr>
                    </thead>
                </table>

                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>


            
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    @endsection


    @section('scripts')
      <script src="{{ asset('plugins/datatables/datatables.js') }}"></script>

          <script>
   $(function() {

 // var id_pers = 0;
  $('#tableStud').dataTable({
    processing: true,
    fixedColumns: true,
    //serverSide: true
    ajax: '/Blocked_S',
  "columnDefs": 
  [
            {
                targets: 5,
                render: function(data, type, full, meta){
                   if(data === 'T')
                   {               
                      return '<span class="badge badge-success">Зашел</span>';
                   } 
                   else 
                   {
                      return '<span class="badge badge-warning">Вышел</span>';
                   }
                }
           }
           
   ],
    //Заменить с eng на ru
    language:
    {
            "lengthMenu": "Выводить _MENU_ записей на страницу",
            "zeroRecords": "Ничего не найдено, извините",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Нет данных",
            "infoFiltered": "(фильтр по _MAX_ кол-ву записей)",
            "search": "Поиск"
    },
  });

   $('#tableWorker').dataTable({
    processing: true,
    //serverSide: true
    ajax: '/Blocked_W',
 
  "columnDefs": 
  [
        
            {
                targets: 5,
                render: function(data, type, full, meta){
                   if(data === 'T')
                   {               
                      return '<span class="badge badge-success">Зашел</span>';
                   } 
                   else 
                   {
                      return '<span class="badge badge-warning">Вышел</span>';
                   }
                }
           }

   ],

    //Заменить с eng на ru
    language:
    {
            "lengthMenu": "Выводить _MENU_ записей на страницу",
            "zeroRecords": "Ничего не найдено, извините",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Нет данных",
            "infoFiltered": "(фильтр по _MAX_ кол-ву записей)",
            "search": "Поиск"
    },
  });
  
        $('#tableImm').dataTable({
    processing: true,
    //serverSide: true
    ajax: '/Blocked_I',
 
  "columnDefs": 
  [
            {
                targets: 3,
                render: function(data, type, full, meta){
                   if(data === 'T')
                   {               
                      return '<span class="badge badge-success">Зашел</span>';
                   } 
                   else 
                   {
                      return '<span class="badge badge-warning">Вышел</span>';
                   }
                }
           }
   ],
    //Заменить с eng на ru
    language:
    {
            "lengthMenu": "Выводить _MENU_ записей на страницу",
            "zeroRecords": "Ничего не найдено, извините",
            "info": "Показано страниц _PAGE_ из _PAGES_",
            "infoEmpty": "Нет данных",
            "infoFiltered": "(фильтр по _MAX_ кол-ву записей)",
            "search": "Поиск"
    },



 
  });
});

</script>
    @endsection
  

