
@extends('layout.application')

@section('content')
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
            <!-- DONUT CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Общая численность по общежитиям</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- DONUT CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Общая численность сотрудников</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart_2" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->


            <!-- PIE CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Общая численность заблокированных</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col (LEFT) -->
          <div class="col-md-6">
            <!-- LINE CHART -->
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Общая численность переселенцев</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="donutChart_3" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- BAR CHART -->
            <div class="card card-success">
              <div class="card-header">
                <h3 class="card-title">Общая численность посещений по месяцам</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>
          <!-- /.col (RIGHT) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    @endsection

@section('scripts')
<!-- ChartJS -->
<script src="{{asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- page script -->
<script>
   $(function () {

    var areaChartData = {
      labels  : [ 'Март', 'Апр', 'Мая', 'Июнь', 'Июль','Авг','Сен','Окт','Ноя','Дек','Янв'],
      datasets: [
        {
          label               : 'Зашло всего за месяц',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius         : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                :
          [

              {!! json_encode($count_input_mart) !!},
              {!! json_encode($count_input_apr) !!},
              {!! json_encode($count_input_may) !!},
              {!! json_encode($count_input_jun) !!},
              {!! json_encode($count_input_jul) !!},
              {!! json_encode($count_input_aug) !!},
              {!! json_encode($count_input_sen) !!},
              {!! json_encode($count_input_oct) !!}
             
          ]
        },
        {
          label               : 'Вышло всего за месяц',
          backgroundColor     : 'rgba(210, 214, 222, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                :
          [
              {!! json_encode($count_output_mart) !!},
              {!! json_encode($count_output_apr) !!},
              {!! json_encode($count_output_may) !!},
              {!! json_encode($count_output_jun) !!},
              {!! json_encode($count_output_jul) !!},
              {!! json_encode($count_output_aug) !!},
              {!! json_encode($count_output_sen) !!},
              {!! json_encode($count_output_oct) !!}
          ]
        },
      ]
    }

    var areaChartOptions = {
      maintainAspectRatio : false,
      responsive : true,
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines : {
            display : false,
          }
        }],
        yAxes: [{
          gridLines : {
            display : false,
          }
        }]
      }
    }

    var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Общежитие №2',
          'Общежитие №3',
          'Общежитие №4',
          'Общежитие №5',
          'Общежитие №7'
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($chart_all_obsh_2) !!},
              {!! json_encode($chart_all_obsh_3) !!},
              {!! json_encode($chart_all_obsh_4) !!},
              {!! json_encode($chart_all_obsh_5) !!},
              {!! json_encode($chart_all_obsh_7) !!}
          ],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }

    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })


    var donutChartCanvas = $('#donutChart_2').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Общежитие №2',
          'Общежитие №3',
          'Общежитие №4',
          'Общежитие №5',
          'Общежитие №7'
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($chart_sotr_obsh_2) !!},
              {!! json_encode($chart_sotr_obsh_3) !!},
              {!! json_encode($chart_sotr_obsh_4) !!},
              {!! json_encode($chart_sotr_obsh_5) !!},
              {!! json_encode($chart_sotr_obsh_7) !!}
          ],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })


      var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })


    var donutChartCanvas = $('#donutChart_3').get(0).getContext('2d')
    var donutData        = {
      labels: [
          'Общежитие №2',
          'Общежитие №3',
          'Общежитие №4',
          'Общежитие №5',
          'Общежитие №7'
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($chart_imm_obsh_2) !!},
              {!! json_encode($chart_imm_obsh_3) !!},
              {!! json_encode($chart_imm_obsh_4) !!},
              {!! json_encode($chart_imm_obsh_5) !!},
              {!! json_encode($chart_imm_obsh_7) !!}
          ],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var donutOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var donutChart = new Chart(donutChartCanvas, {
      type: 'doughnut',
      data: donutData,
      options: donutOptions
    })


    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
    var pieData        =
    {
      labels: [
          'Заблокированные в Общежитие №2',
          'Заблокированные в Общежитие №3',
          'Заблокированные в Общежитие №4',
          'Заблокированные в Общежитие №5',
          'Заблокированные в Общежитие №7'
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($chart_blocked_obsh_2) !!},
              {!! json_encode($chart_blocked_obsh_3) !!},
              {!! json_encode($chart_blocked_obsh_4) !!},
              {!! json_encode($chart_blocked_obsh_5) !!},
              {!! json_encode($chart_blocked_obsh_7) !!}
          ],
          backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
        }
      ]
    }
    var pieOptions     = {
      maintainAspectRatio : false,
      responsive : true,
    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var pieChart = new Chart(pieChartCanvas, {
      type: 'pie',
      data: pieData,
      options: pieOptions
    })

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    var barChart = new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })


  })
</script>
@endsection
