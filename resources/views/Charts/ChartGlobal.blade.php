
@extends('layout.application')

@section('content')
   <section class="content">
     <div class="container-fluid">
       <h5 class="mt-4 mb-2">ОБЩЕЖИТИЕ №2</h5>
      <div class="row">
       
          <div class="col-md-3 col-sm-6 col-12">
           <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Студенты</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_2_S" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Сотрудники</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_2_W" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
           <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Переселенцы</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_2_I" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>

         <h5 class="mt-4 mb-2">ОБЩЕЖИТИЕ №3</h5>
      <div class="row">
       
          <div class="col-md-3 col-sm-6 col-12">
           <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Студенты</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_3_S" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Сотрудники</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_3_W" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Переселенцы</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_3_I" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>

         <h5 class="mt-4 mb-2">ОБЩЕЖИТИЕ №4</h5>
      <div class="row">
       
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Студенты</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_4_S" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
           
           <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Сотрудники</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_4_W" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
          </div>
          <!-- /.col -->
        </div>

          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Переселенцы</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_4_I" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>

        <h5 class="mt-4 mb-2">ОБЩЕЖИТИЕ №5</h5>
      <div class="row">
       
          <div class="col-md-3 col-sm-6 col-12">
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Студенты</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_5_S" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
             <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Сотрудники</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_5_W" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Переселенцы</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_5_I" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <h5 class="mt-4 mb-2">ОБЩЕЖИТИЕ №7</h5>
      <div class="row">
       
          <div class="col-md-3 col-sm-6 col-12">
          <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Студенты</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_7_S" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Сотрудники</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_7_W" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          <!-- /.col -->
          <div class="col-md-3 col-sm-6 col-12">
            <div class="card card-danger">
              <div class="card-header">
                <h3 class="card-title">Переселнцы</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                <canvas id="hostel_7_I" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>



      </div>
    </section>

    @endsection

@section('scripts')

<!-- ChartJS -->
<script src="{{ asset('plugins/chart.js/Chart.min.js') }}"></script>
<!-- page script -->
<script>
   $(function () {

   var pieChart_Hostel_2_S = $('#hostel_2_S').get(0).getContext('2d')
   var pieData_2_S       =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_2_S_T) !!}',
          'Вышло: {!! json_encode($hostel_2_S_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_2_S_T) !!},
              {!! json_encode($hostel_2_S_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_2_S = 
    new Chart(pieChart_Hostel_2_S, 
    {
      type: 'pie',
      data: pieData_2_S,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })
    //-------------------------2------------------------------------------//

       var pieChart_Hostel_2_W = $('#hostel_2_W').get(0).getContext('2d')
   var pieData_2_W        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_2_W_T) !!}',
          'Вышло: {!! json_encode($hostel_2_W_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_2_W_T) !!},
              {!! json_encode($hostel_2_W_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_2_W = 
    new Chart(pieChart_Hostel_2_W, 
    {
      type: 'pie',
      data: pieData_2_W,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })
//---------------------------2-------------------------------------------------
  
      var pieChart_Hostel_2_I = $('#hostel_2_I').get(0).getContext('2d')
   var pieData_2_I        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_2_I_T) !!}',
          'Вышло: {!! json_encode($hostel_2_I_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_2_I_T) !!},
              {!! json_encode($hostel_2_I_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_2_I = 
    new Chart(pieChart_Hostel_2_I, 
    {
      type: 'pie',
      data: pieData_2_I,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

    //----------------------3------------------------------------------------------
  
      var pieChart_Hostel_3_S = $('#hostel_3_S').get(0).getContext('2d')
   var pieData_3_S        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_3_S_T) !!}',
          'Вышло: {!! json_encode($hostel_3_S_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_3_S_T) !!},
              {!! json_encode($hostel_3_S_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_3_S = 
    new Chart(pieChart_Hostel_3_S, 
    {
      type: 'pie',
      data: pieData_3_S,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

     //----------------------3------------------------------------------------------
  
      var pieChart_Hostel_3_W = $('#hostel_3_W').get(0).getContext('2d')
   var pieData_3_W        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_3_W_T) !!}',
          'Вышло: {!! json_encode($hostel_3_W_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_3_W_T) !!},
              {!! json_encode($hostel_3_W_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_3_W = 
    new Chart(pieChart_Hostel_3_W, 
    {
      type: 'pie',
      data: pieData_3_W,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

         //----------------------3------------------------------------------------------
  
      var pieChart_Hostel_3_I = $('#hostel_3_I').get(0).getContext('2d')
   var pieData_3_I        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_3_I_T) !!}',
          'Вышло: {!! json_encode($hostel_3_I_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_3_I_T) !!},
              {!! json_encode($hostel_3_I_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_3_I = 
    new Chart(pieChart_Hostel_3_I, 
    {
      type: 'pie',
      data: pieData_3_I,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

     //--------------------4--------------------------------------------------------
  
      var pieChart_Hostel_4_S = $('#hostel_4_S').get(0).getContext('2d')
   var pieData_4_S        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_4_S_T) !!}',
          'Вышло: {!! json_encode($hostel_4_S_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_4_S_T) !!},
              {!! json_encode($hostel_4_S_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_4_S = 
    new Chart(pieChart_Hostel_4_S, 
    {
      type: 'pie',
      data: pieData_4_S,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

  //--------------------4--------------------------------------------------------
  
     var pieChart_Hostel_4_W = $('#hostel_4_W').get(0).getContext('2d')
   var pieData_4_W        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_4_W_T) !!}',
          'Вышло: {!! json_encode($hostel_4_W_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_4_W_T) !!},
              {!! json_encode($hostel_4_W_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_4_W = 
    new Chart(pieChart_Hostel_4_W, 
    {
      type: 'pie',
      data: pieData_4_W,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })


      //--------------------4--------------------------------------------------------
  
       var pieChart_Hostel_4_I = $('#hostel_4_I').get(0).getContext('2d')
   var pieData_4_I        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_4_I_T) !!}',
          'Вышло: {!! json_encode($hostel_4_I_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_4_I_T) !!},
              {!! json_encode($hostel_4_I_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_4_I = 
    new Chart(pieChart_Hostel_4_I, 
    {
      type: 'pie',
      data: pieData_4_I,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

    //--------------------5--------------------------------------------------------
  
   var pieChart_Hostel_5_S = $('#hostel_5_S').get(0).getContext('2d')
   var pieData_5_S        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_5_S_T) !!}',
          'Вышло: {!! json_encode($hostel_5_S_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_5_S_T) !!},
              {!! json_encode($hostel_5_S_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_5_S = 
    new Chart(pieChart_Hostel_5_S, 
    {
      type: 'pie',
      data: pieData_5_S,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })


    //--------------------5--------------------------------------------------------
  
   var pieChart_Hostel_5_W = $('#hostel_5_W').get(0).getContext('2d')
   var pieData_5_W        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_5_W_T) !!}',
          'Вышло: {!! json_encode($hostel_5_W_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_5_W_T) !!},
              {!! json_encode($hostel_5_W_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_5_W = 
    new Chart(pieChart_Hostel_5_W, 
    {
      type: 'pie',
      data: pieData_5_W,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })


     //--------------------5--------------------------------------------------------
  
   var pieChart_Hostel_5_I = $('#hostel_5_I').get(0).getContext('2d')
   var pieData_5_I        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_5_I_T) !!}',
          'Вышло: {!! json_encode($hostel_5_I_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_5_I_T) !!},
              {!! json_encode($hostel_5_I_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_5_I = 
    new Chart(pieChart_Hostel_5_I, 
    {
      type: 'pie',
      data: pieData_5_I,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

       //--------------------7--------------------------------------------------------
  
   var pieChart_Hostel_7_S = $('#hostel_7_S').get(0).getContext('2d')
   var pieData_7_S        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_7_S_T) !!}',
          'Вышло: {!! json_encode($hostel_7_S_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_7_S_T) !!},
              {!! json_encode($hostel_7_S_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_7_S = 
    new Chart(pieChart_Hostel_7_S, 
    {
      type: 'pie',
      data: pieData_7_S,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })

    //--------------------7--------------------------------------------------------
  
   var pieChart_Hostel_7_W = $('#hostel_7_W').get(0).getContext('2d')
   var pieData_7_W        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_7_W_T) !!}',
          'Вышло: {!! json_encode($hostel_7_W_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_7_W_T) !!},
              {!! json_encode($hostel_7_W_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_7_W = 
    new Chart(pieChart_Hostel_7_W, 
    {
      type: 'pie',
      data: pieData_7_W,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })
    //------------------------7-----------------------------------------
    
    var pieChart_Hostel_7_I = $('#hostel_7_I').get(0).getContext('2d')
   var pieData_7_I        =
    {
      labels: [
          'Зашло: {!! json_encode($hostel_7_I_T) !!}',
          'Вышло: {!! json_encode($hostel_7_I_F) !!}'
       
      ],
      datasets: [
        {
          data:
          [
              {!! json_encode($hostel_7_I_T) !!},
              {!! json_encode($hostel_7_I_F) !!}
          ],
          backgroundColor : ['#00a65a', '#f39c12'],
        }
      ]
    }

    var pieChart_7_I = 
    new Chart(pieChart_Hostel_7_I, 
    {
      type: 'pie',
      data: pieData_7_I,
      options:
      {
        maintainAspectRatio : false,
        responsive : true,
      } 
    })
    


 })
 
</script>
@endsection
