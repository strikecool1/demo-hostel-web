@extends('layout.application')

@section('content')
    <!-- Main content -->
      <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$all_student}}</h3>

                <p>Общая численность студентов в БД</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ $all_person }}</h3>
                <p>Общая численность заселённых в БД</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ $all_blocked }}</h3>

                <p>Заблокированных</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $hostel_2_T }}/{{ $hostel_2_F }}</h3>

                <p>ОБЩЕЖИТИЕ №2</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

              <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $hostel_3_T }}/{{ $hostel_3_F }}</h3>

                <p>ОБЩЕЖИТИЕ №3</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $hostel_4_T }}/{{ $hostel_4_F }}</h3>

                <p>ОБЩЕЖИТИЕ №4</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $hostel_5_T }}/{{ $hostel_5_F }}</h3>

                <p>ОБЩЕЖИТИЕ №5</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $hostel_7_T }}/{{ $hostel_7_F }}</h3>

                <p>ОБЩЕЖИТИЕ №7</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="table-responsive">
                  <table class="table m-0">
                    <thead>
                    <tr>
                      <th>Время посещения</th>
                      <th>ФИО</th>
                      <th>Статус</th>
                      <th>Общежитие</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($row_move as $move_stud)
                    <tr>

                      @if($move_stud->Date_Move != null)
                      <td>{{ date('d.m.Y H:i:s', strtotime($move_stud->Date_Move)) }}</td>
                      @else <td>Данные отсутствуют</td>
                      @endif
                      
                      <td>{{ $move_stud->FirstName }} {{ $move_stud->Name }} {{ $move_stud->LastName }}</td>
                      @if($move_stud->status == 'Зашел')
                      <td><span class="badge badge-success">Зашел</span></td>
                      @else <td><span class="badge badge-warning">Вышел</span></td>
                      @endif
                      <td>
                        {{ $move_stud->Building }}
                      </td>
                    </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.table-responsive -->
              
                </div>
                <!-- /.row -->
              </div>
              <!-- ./card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @endsection

 
