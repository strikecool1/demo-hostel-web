@extends('layout.application')

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                      <img class=" img-fluid" src="data:image/png;base64,{{ base64_encode($person_file->photo) }}">
                </div>

                <h3 class="profile-username text-center">
                 {{ $person_file->FirstName }} {{ $person_file->Name }} {{ $person_file->LastName }}</h3>

                <p class="text-muted text-center">{{ $person_file->GroupName }} {{ $person_file->numberCourse }}</p>
                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Возраст</b> <a class="float-right">{{ $date_person }}</a>
                  </li>
                  <li class="list-group-item">
                    @if($person_file->status == "F")
                    <b>Статус</b> <a class="float-right">Вышел</a>
                    @else
                     <b>Статус</b> <a class="float-right">Зашел</a>
                     @endif
                  </li>
                  <li class="list-group-item">
                    <b>Комната</b> <a class="float-right">{{ $person_file->numberRoom }}</a>
                  </li>
                </ul>

                <a href="/Print_QR" class="btn btn-primary btn-block" target="_blank"><i class="fas fa-print"></i><b>QR-code</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item "><a class="nav-link active" href="#timeline" data-toggle="tab">История перемещения</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Данные</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">


                      @foreach($move_ as $item)

                      @if($item->type_status == "Зашел" && empty($item->date_move) != true)
                          <div class="time-label">
                            <span class="bg-success">
                             {{ $item->date_move }}
                            </span>
                          </div>
                           <div>
                            <i class="far fa-clock bg-gray"></i>
                           <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i></span>
                              <h3 class="timeline-header border-0">{{ $item->type_status }}</h3>
                            </div>
                          </div>

                          @elseif(empty($item->date_move) != true)

                          <div class="time-label">
                            <span class="bg-warning">
                             {{ $item->date_move }}
                            </span>
                          </div>
                           <div>
                            <i class="far fa-clock bg-gray"></i>
                           <div class="timeline-item">
                              <span class="time"><i class="far fa-clock"></i></span>
                              <h3 class="timeline-header border-0">{{ $item->type_status }}</h3>
                            </div>
                          </div>
                          @else
                            <div class="time-label">
                            <span class="bg-danger">
                            Данные отсутствуют
                            </span>
                          </div>
                           @endif
                      @endforeach
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane " id="settings">
                    <!-- Форма заполнения для update -->
                    <form class="form-horizontal" method="POST" action="">
                      <div class="form-group row">
                        <label for="inputFitsName" class="col-sm-2 col-form-label">Фамилия</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputFitsName" value="{{ $person_file->FirstName }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Имя</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" value="{{ $person_file->Name }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputLastName" class="col-sm-2 col-form-label">Отчество</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputLastName" value="{{ $person_file->LastName }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputInst" class="col-sm-2 col-form-label">Институт</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputInst" value="{{ $person_file->Inst }}">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSpes" class="col-sm-2 col-form-label">Специальность</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSpes" value="{{ $person_file->Spes }}">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputPropusk" class="col-sm-2 col-form-label">Номер пропуска</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputPropusk" >{{ $person_file->num_propusk }}</textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                       <div class="input-group sm-2">
                          <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01">Заблокирован</label>
                          </div>

                          <select class="custom-select" id="inputGroupSelect01">
                             @if($person_file->block =="F")
                            <option selected>Нет</option>
                            <option value="T">Да</option>
                            @else
                            <option selected>Да</option>
                            <option value="F">Нет</option>
                            @endif
                          </select>
                        </div>
                      </div>

                    </form>


                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    @endsection

    @section('scripts')
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    @endsection
