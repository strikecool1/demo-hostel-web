 
  <?php $routeName = Route::currentRouteName(); ?>

  <aside class="main-sidebar elevation-4 sidebar-light-olive">
    <!-- Brand Logo -->
    <a href="/Main" class="brand-link">
      <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Admin Hostel </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="/Main" class="d-block">С возвращением, {{$user_name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-compact" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview {{ strpos($routeName, 'main.') === 0 ? ' menu-open ' : '' }}">

            <a href="#" class="nav-link {{ strpos($routeName, 'main.') === 0 ? ' active ' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Основная информация
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
               <a href="{{ route('main.Main') }}" class="nav-link {{ strpos($routeName, 'main.Main') === 0 ? ' active ' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Главное</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview {{ strpos($routeName, 'charts.') === 0 ? ' menu-open ' : '' }}">
            <a href="#" class="nav-link {{ strpos($routeName, 'charts.') === 0 ? ' active ' : '' }}">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Статистика
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('charts.chartMain') }}" class="nav-link {{ strpos($routeName, 'charts.chartMain') === 0 ? ' active ' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p> Общая численность</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('charts.chartGlobal') }}" class="nav-link {{ strpos($routeName, 'charts.chartGlobal') === 0 ? ' active ' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Подробная статистика</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ route('charts.chartGlobal') }}" class="nav-link {{ strpos($routeName, 'charts.chartGlobal') === 0 ? ' active ' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Списки отсутсвующих</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview {{ strpos($routeName, 'blocked.') === 0 ? ' menu-open ' : '' }}">
            <a href="{{ route('blocked.table_blocked') }}" class="nav-link {{ strpos($routeName, 'blocked.table_blocked') === 0 ? ' active ' : '' }}">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Заблокированные
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>

           <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.') === 0 ? ' menu-open ' : '' }}">
            <a href="#" class="nav-link {{ strpos($routeName, 'hostels.') === 0 ? ' active ' : '' }}">
              <i class="nav-icon fas fa-tree"></i>
              <p>
                Список общежитий
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.Hostel_2') === 0 ? ' menu-open ' : '' }}">
                <a href="#" class="nav-link {{ strpos($routeName, 'hostels.Hostel_2') === 0 ? ' active ' : '' }}" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Общежитие №2
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_2.S') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_2.S') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Студенты</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_2.W') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_2.W') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Сотрудники</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_2.I') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_2.I') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Переселенцы</p>
                    </a>
                  </li>
                </ul>
              </li>

                <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.Hostel_3') === 0 ? ' menu-open ' : '' }}">
                <a href="#" class="nav-link {{ strpos($routeName, 'hostels.Hostel_3') === 0 ? ' active ' : '' }}" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Общежитие №3
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_3.S') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_3.S') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Студенты</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_3.W') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_3.W') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Сотрудники</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_3.I') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_3.I') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Переселенцы</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.Hostel_4') === 0 ? ' menu-open ' : '' }}">
                <a href="#" class="nav-link {{ strpos($routeName, 'hostels.Hostel_4') === 0 ? ' active ' : '' }}" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Общежитие №4
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_4.S') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_4.S') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Студенты</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_4.W') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_4.W') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Сотрудники</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_4.I') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_4.I') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Переселенцы</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.Hostel_5') === 0 ? ' menu-open ' : '' }}">
                <a href="#" class="nav-link {{ strpos($routeName, 'hostels.Hostel_5') === 0 ? ' active ' : '' }}" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Общежитие №5
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_5.S') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_5.S') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Студенты</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_5.W') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_5.W') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Сотрудники</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_5.I') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_5.I') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Переселенцы</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview {{ strpos($routeName, 'hostels.Hostel_7') === 0 ? ' menu-open ' : '' }}">
                <a href="#" class="nav-link {{ strpos($routeName, 'hostels.Hostel_7') === 0 ? ' active ' : '' }}" >
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Общежитие №7
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_7.S') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_7.S') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Студенты</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_7.W') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_7.W') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Сотрудники</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('hostels.Hostel_7.I') }}" class="nav-link {{ strpos($routeName, 'hostels.Hostel_7.I') === 0 ? ' active ' : '' }}">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Переселенцы</p>
                    </a>
                  </li>
                </ul>
              </li>



            </ul>
          </li>

            <!--<li class="nav-item has-treeview {{ strpos($routeName, 'manager.') === 0 ? ' menu-open ' : '' }}">
            <a href="#" class="nav-link {{ strpos($routeName, 'manager.') === 0 ? ' active ' : '' }}">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Менеджер
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/Table_Blocked" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Списки пользователей</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/Table_Blocked" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Создать пользователя</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="/Table_Blocked" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Создать новую комнату</p>
                </a>
              </li>
            </ul>
          </li>-->

      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
