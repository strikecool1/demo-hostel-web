@extends('layout.application')

@section('styles')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
   <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Список проживающих студентов</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="tableStud" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Ф.И.О.</th>
                   <th>Секция</th>
                    <th>Комната</th>
                    <th>Институт</th>
                    <th>Группа</th>
                    <th>Статус</th>
                    <th>Последняя активиность</th>
                    <th>Действия</th>
                </tr>
                </thead>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    @endsection

    @section('scripts')
     <!-- DataTables -->
    <script src="{{ asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

    <script>
  $(function () {
    $('#tableStud').DataTable
    (
      {
        ajax: '/Hostel_7_S',
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,

        "columnDefs": 
          [
            {
                targets: 5,
                render: function(data, type, full, meta){
                   if(data === 'T')
                   {               
                      return '<span class="badge badge-success">Зашел</span>';
                   } 
                   else 
                   {
                      return '<span class="badge badge-warning">Вышел</span>';
                   }
                }
           }
           
        ],
        language:
        {
              "processing": "Подождите...",
              "search": "Поиск:",
              "lengthMenu": "Показать _MENU_ записей",
              "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
              "infoEmpty": "Записи с 0 до 0 из 0 записей",
              "infoFiltered": "(отфильтровано из _MAX_ записей)",
              "infoPostFix": "",
              "loadingRecords": "Загрузка записей...",
              "zeroRecords": "Записи отсутствуют.",
              "emptyTable": "В таблице отсутствуют данные",
              "paginate": 
              {
                "first": "Первая",
                "previous": "Предыдущая",
                "next": "Следующая",
                "last": "Последняя"
              },
              "aria": 
              {
                "sortAscending": ": активировать для сортировки столбца по возрастанию",
                "sortDescending": ": активировать для сортировки столбца по убыванию"
              },
              "select": 
              {
                "rows": 
                {
                  "_": "Выбрано записей: %d",
                  "0": "Кликните по записи для выбора",
                  "1": "Выбрана одна запись"
                }
              }
        }


      }
    );
  });
</script>

    @endsection
  

