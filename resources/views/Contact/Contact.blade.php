@extends('layout.application')

@section('content')
 <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Отдел "АСУ ВУЗ"
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h1 class="lead"><b>Ливинский П.С. </b></h1>
                      <p class="text-muted text-sm"><b>Отдел: </b> Цент информационных технологий / Отдел автоматизации и управления университетом / Сектор программирования</p>
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Адрес: 1 корпус/ 5 этаж/ 600 каб.</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Телефон : 16-00</li>
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                      <img src="../../dist/img/user1-128x128.jpg" alt="" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                    <a href="#" class="btn btn-sm bg-teal">
                      <i class="fas fa-comments">Отправить сообщение</i>
                    </a>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /.card-body -->
     
      </div>
      <!-- /.card -->

    </section>

    @endsection
