FROM composer:2.1.2 AS composer
#composer:2.0.0-alpha3
#
FROM php:5.6-apache
#synctree/mediawiki:1.24
#php:5.6-apache
# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6 \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install mysqli pdo pdo_mysql mbstring zip exif pcntl

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www/Hostels
COPY . ./
RUN composer update
RUN php artisan key:generate
CMD composer update && composer update kylekatarnls/update-helper && composer install && php artisan serve --host=0.0.0.0 --port=8000
